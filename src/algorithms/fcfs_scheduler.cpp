#include "fcfs_scheduler.h"

using namespace std;

/**
 * Returns the next thread that should be executed, or NULL if there is no
 * thread available to run.
 */
SchedulingDecision* FcfsScheduler::get_next_thread(const Event* event) {
    // Is there any Thread Ready?
    if (ready_queue.empty()){
        return nullptr;
    }

    // Generate a New Scheduling decision
    SchedulingDecision* decision = new SchedulingDecision;
    Thread* the_choosen_one = ready_queue.front();
    ready_queue.pop();
    decision->thread = the_choosen_one;


    // Create a explanation
    decision->explanation = "    Selected from ";
    decision->explanation += to_string(this->size() + 1);
    decision->explanation += " threads; will run to completion of burst";

    // Update size()
    return decision;
}

/**
 * Enqueues the given thread in this scheduler's ready queues.
 */
void FcfsScheduler::enqueue(const Event* event, Thread* thread) {
    ready_queue.emplace(thread);
}

/**
 * Returns true if the currently running thread should be preempted upon the
 * arrival of the given thread, as represented by event.
 */
bool FcfsScheduler::should_preempt_on_arrival(const Event* event) const {
    return false;
}

/**
 * Returns the number of threads in this scheduler's ready queues.
 */
size_t FcfsScheduler::size() const {
    return ready_queue.size();
}
