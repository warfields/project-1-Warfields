#include "types/thread.h"

using namespace std;

// TODO: implement the behavior for your thread methods (optional)

// Updates current state and 
void Thread::update_state(Thread::State new_state, int time){
    previous_state = current_state;
    current_state = new_state;
    state_change_time = time;
}