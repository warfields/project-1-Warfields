#include "types/system_stats.h"

using namespace std;

/*
def wellfordsavg(prev, x_n, n):
    return prev + (x_n - prev) / n
*/
double welfords(double prev, double x_n, int n) {
    return prev + (x_n - prev) / (float) n;
}


void SystemStats::run_util_calc(){
    // CPU Utilization
    cpu_utilization = (double) (service_time + dispatch_time) / (double) total_time * 100;

    // CPU Efficency
    cpu_efficiency = (double) service_time / (double) total_time * 100;
}

// Use welfords averages
void SystemStats::run_thread_calcs(std::map<int, Process*> processes){
    int n[4] = {1,1,1,1};
    auto r_prev = avg_thread_response_times;
    auto t_prev = avg_thread_turnaround_times;

    for (auto proc : processes){
        for (auto thread : proc.second->threads){
            int type = thread->process_type;
            // Average Thread Responce Time
            r_prev[type] = welfords(r_prev[type],thread->response_time(), n[type]);

            // Average Thread Turnaround Time
            t_prev[type] = welfords(t_prev[type],thread->turnaround_time(), n[type]);

            // Update Other Stats
            service_time += thread->service_time;
            io_time += thread->io_time;
            n[type] += 1;

        }
    }

    total_cpu_time = service_time + dispatch_time; 
    total_idle_time = total_time - total_cpu_time;

}

void SystemStats::run_all_calcs(std::map<int, Process*> processes){
    run_thread_calcs(processes);
    run_util_calc();
}